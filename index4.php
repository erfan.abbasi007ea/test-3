<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="base.css">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="jquery-3.5.1.min.js"></script>
    <title>Ajax Test</title>
</head>
<body>
<div id="header-wrapper"></div>

<div id="content" class="ltr">

    <ul class="cols">
        <li class="w50"><input style="width: 300px;" id="search" placeholder="Type keywords here:" autocomplete="off">
        </li>
        <li class="w50">
            <div id="preview"></div>
        </li>
    </ul>
</div>

</body>
</html>

<script>
    $(function () {
        $('#search').on('keyup', function () {
            var value = $(this).val();
            $.ajax('feed.php', {
                type: 'post',
                dataType: 'json',
                data: {
                    keyword: value
                },
                success: function (data) {
                    $("#preview").text(data['html']);
                },
            });
        });
    });
</script>
