<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="base.css">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="jquery-3.5.1.min.js"></script>
    <title>javascript test</title>
</head>
<body>
<div id="header-wrapper"></div>

<div id="content" class="ltr">

    <ul class="cols">
        <li class="w50"><textarea style="width: 300px; height: 300px" id="html-source" placeholder="Type HTML source:"
                                  value="Test" autocomplete="off"></textarea></li>
        <li class="w50">
            <div id="preview"></div>
        </li>
    </ul>
</div>

</body>
</html>

<script>

    $(function () {
        $('#html-source').on('keyup', function () {
            var value = $(this).val();
            $('#preview').html(value);
        });

        var btn = $('#target-btn');
        btn.on('click', function () {
            alert("A");
        });
    });

</script>
