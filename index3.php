<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="base.css">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="jquery-3.5.1.min.js"></script>
    <title>javascript test</title>
</head>
<body>
<div id="header-wrapper"><br></div>

<div id="content" class="ltr">
    <span class="colored red">Sample 1</span>
    <span class="colored blue">Sample 2</span>
    <span class="colored green">Sample 3</span>
    <span class="colored yellow">Sample 4</span>
    <span class="colored pink">Sample 5</span>
    <br>
    <br>
    <div id="comments"></div>
    <br>
    <br>

    <div>Something</div>
    <div>Something</div>
    <div>Something</div>
    <div>Something</div>
    <div>Something</div>
    <div>Something</div>
    <div>Something</div>
    <div>Something</div>


    <span class="btn-blue" id="add-Comment">Add Comment</span>
    <br>
    <br>
    <br>
    <div id="sticky">My name is Sirish</div>
</div>

</body>
</html>

<script>

    $(function () {
        $('.red').each(function () {
            $(this).css('color', '#f00');
        });
        $('.blue').each(function () {
            $(this).css('color', '#00f');
        });
        $('.green').each(function () {
            $(this).css('color', '#0f0');
        });
        $('.yellow').each(function () {
            $(this).css('color', '#ff0');
        });
        $('.pink').each(function () {
            $(this).css('color', '#f0f');
        });

        $('.colored').each(function () {
            if ($(this).css('color') == 'rgb(255, 255, 0)') {
                $(this).css('background-color', '#f09');
            }
        });

        $('span').css('padding', '10px');
        $('.colored').on('mouseover', function () {
            $(this).data('oldColor', $(this).css('background-color'));
            $(this).css('background-color', '#000');
        });
        $('.colored').on('mouseout', function () {
            var oldColor = $(this).data('oldColor');
            $(this).css('background-color', oldColor);
        });
        $('#add-Comment').on('click', function () {
            var $div = $('<div style="line-height: 30px"></div>');
            var $removeBtn = $('<span class="btn">Remove</span>');
            var $commentBody = $('<span>This is a comment</span>');
            $removeBtn.on('click', function () {
                alert('A');
            });
            $div.append($removeBtn);
            $div.append($commentBody);


            $('#comments').append($div);
        });

        $(window).on('scroll', function () {
            var top = $(window).scrollTop();
            $('#sticky').animate({
                top: top + 'px';
            }, 10);
        });
    });
</script>
