<html>
<head lang="en">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="base.css">
    <link rel="stylesheet" href="style.css">
    <script type="text/javascript" src="jquery-3.5.1.min.js"></script>
    <title>javascript test</title>
</head>
<body>
<div id="header-wrapper"></div>

<div id="content" class="ltr">

    <ul class="cols">
        <li class="w50"><input style="width: 300px" id="search" placeholder="Type your keyword:" value=""
                               autocomplete="off"></li>
        <li class="w50">
            <div id="preview"></div>
        </li>
    </ul>
</div>

</body>
</html>

<script>

    $(function () {
        var words = ['hello', 'how are you', 'alarm', 'abort', 'Axe', 'plus', 'processor', 'cat', 'car', 'cow', 'row', 'accessor'];

        $('#search').on('keyup', function () {
            var value = $('#search').val();
            var matches = [];
            words.forEach(function (item) {
                if (item.indexOf(value) > -1) {
                    matches.push(item);
                }
            });

            var html = "";
            matches.forEach(function (item) {
                html += item + "<br>";
            });
            $('#preview').html(html);
        });
    });
</script>
